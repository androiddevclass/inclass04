package com.example.advait.inclass04;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;

import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MainActivity extends AppCompatActivity {

    Handler handler;

    private static final String TAG = "MainActivity";
    public static final int STATUS_START=0x00;
    public static final int STATUS_WORK=0x01;
    public static final int STATUS_END=0x02;

    private ExecutorService executorService = Executors.newFixedThreadPool(2);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate: init");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViewById(R.id.generatePasswordsThreadbutton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int len = ((SeekBar) findViewById(R.id.seekBar2)).getProgress()+8;
                int count = ((SeekBar) findViewById(R.id.seekBar1)).getProgress()+1;
                executorService.execute(new DoWork(len,count));
                executorService.execute(new DoWork(len,count));
            }
        });

        findViewById(R.id.generatePasswordsAsyncButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: starting async");
                new DoAsyncWork().execute(0);
            }
        });
        Log.d(TAG, "onCreate: listners set to buttons");

        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.progressBarMrssage));
        progressDialog.setMax(100);
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);

        handler = new Handler(new Handler.Callback() {
            @Override
            public boolean handleMessage(Message msg) {
                switch (msg.what){
                    case STATUS_START:
                        Log.d(TAG, "handleMessage: started progress");
                        progressDialog.show();
                        break;
                    case STATUS_WORK:
                        progressDialog.setProgress((int) msg.obj);
                        break;
                    case STATUS_END:
                        progressDialog.dismiss();
                        showAndGetPasswords((String[]) msg.obj);
                        break;
                }
                return true;
            }
        });

        ((SeekBar)findViewById(R.id.seekBar1)).setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                ((TextView) findViewById(R.id.passwordCountResultTextView)).setText(progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        ((SeekBar)findViewById(R.id.seekBar2)).setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                ((TextView) findViewById(R.id.passwordLengthResultTextView)).setText(progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    private String showAndGetPasswords(final String[] pwd){

        final String[] selectedPwd = new String[1];
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.alertTitle))
                .setItems(pwd, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        selectedPwd[0] = pwd[which];
                        Log.d(TAG, "onClick: selected "+selectedPwd[0]);
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
        return selectedPwd[1];
    }





    class DoWork implements Runnable{

        int length;
        int count;
        String[] passwords;

        public DoWork(int len, int cnt){
            length=len;
            count=cnt;
        }

        @Override
        public void run() {
            passwords = new String[count];
            Log.d(TAG, "generating passwords");
            //sent init message
            Message message =new Message();
            message.what= MainActivity.STATUS_START;
            handler.sendMessage(message);

            for (int i = 0; i < count; i++) {
                passwords[i] = Util.getPassword(length);
                message =new Message();
                message.what= MainActivity.STATUS_WORK;
                message.obj = i+1;
                handler.sendMessage(message);
            }

            message =new Message();
            message.what= MainActivity.STATUS_END;
            message.obj = passwords;
        }

        public String[] getPasswords(){
            return passwords;
        }
    }



    class DoAsyncWork extends AsyncTask<Integer, Integer, String[]> {

        int length;
        int count;
        String[] passwords;
        ProgressDialog progressDialog = null;

        @Override
        protected void onPreExecute() {
             length = ((SeekBar) findViewById(R.id.seekBar2)).getProgress()+8;
            Log.d(TAG, "onPreExecute: len "+length);
             count = ((SeekBar) findViewById(R.id.seekBar1)).getProgress()+1;
            progressDialog = new ProgressDialog(MainActivity.this);
            progressDialog.setMessage(getString(R.string.progressBarMrssage));
            progressDialog.setMax(100);
            progressDialog.setCancelable(false);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            progressDialog.show();
        }

        @Override
        protected void onPostExecute(String[] strings) {
            progressDialog.dismiss();
            showAndGetPasswords(strings);
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            progressDialog.setProgress(values[0]);
        }

        public String[] getPasswords(){
            return passwords;
        }

        @Override
        protected String[] doInBackground(Integer... params) {
            passwords = new String[count];
            Log.d(TAG, "generating passwords");
            //sent init message
//            Message message =new Message();
//            message.what= MainActivity.STATUS_START;
//            handler.sendMessage(message);

            for (int i = 0; i < count; i++) {
                passwords[i] = Util.getPassword(length);
                publishProgress(i+1);
//                message =new Message();
//                message.what= MainActivity.STATUS_WORK;
//                message.obj = i+1;
//                handler.sendMessage(message);
            }
//
//            message =new Message();
//            message.what= MainActivity.STATUS_END;
//            message.obj = passwords;

            return passwords;
        }
    }

}


